import random, string
getallen = string.digits
genereren = True

while genereren == True:
    bsn= []
    for m in range(9):
        bsn += random.choice(getallen)
    
    calc = (9 * int(bsn[0])) + (8 * int(bsn[1])) +  (7 * int(bsn[2])) + (6 * int(bsn[3])) + (5 * int(bsn[4])) + (4 * int(bsn[5])) + (3 * int(bsn[6])) + (2 * int(bsn[7])) + (-1 * int(bsn[8]))

    if calc % 11 == 0:
        genereren = False
        print("Het gegenereerde BSN-nummer is: ", *bsn, sep='')
